REM This script performs an in-place upgrade from Windows 7 to Windows 10
REM The contents of the Windows 10 iso should be extracted and placed in
REM a folder that is accessible. 
REM Author Sean Young
REM Rev 1.0
REM Oct 2019

:: *********************************************************************
:: Setup;
:: *********************************************************************
REM Declare Variables

:: ########### EDIT THESE VARIABLES ###########
REM Path to Windows 10 iso contents
set "win10Path=\\SERVER\Path\To\Folder"

REM Set the type of upgrade; Clean is a clean install, DataOnly saves data
REM but not apps, Upgrade saves data and apps. This option cannot be used
REM with an unattend file.
REM [Un-comment one, comment the others]
:: set "upgradeType=Clean"
:: set "upgradeType=DataOnly"
set "upgradeType=Upgrade"

REM Set the Dynamic Update switch; This specifies whether updates will be downloaded
REM and installed during setup.
REM [Un-comment one, comment the other]
:: set "dUpdate=disable"
set "dUpdate=enable"

REM Set the Migrate Drivers switch; This specifies whether current drivers will
REM migrate to the new OS or not
REM [Un-comment one, comment the other]
set "mDrivers=all"
:: set "mDrivers=none"

REM Set the Show OOBE switch; The specifies whether the out of box experience (OOBE)
REM will require user interactivity or not.
REM [Un-comment one, comment the other]
:: set "OOBE=full"
set "OOBE=none"

REM Set the telemetry switch; This specifies whether installation data will be reported
REM to Microsoft or not.
REM [Un-comment one, comment the other]
set "telemetry=disable"
:: set "telemetry=enable"

:: ############################################

:: *********************************************************************
:: Begin deployment; You shouldn't have to edit anything below this line
:: *********************************************************************

echo %date% %time% Windows 10 Upgrade started on %computername% >> %win10Path%\upgradelog.txt

start /wait %win10Path%\setup.exe /auto %upgradeType% /migratedrivers %mdrivers% /dynamicupdate %dUpdate% /showoobe %OOBE% /telemetry %telemetry%