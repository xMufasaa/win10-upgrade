# win10_upgrade

Script to perform in-place upgrade from Win7 to Win10

There are many switches and options that can be used other than what is in this script, I just find these to be the
most frequently used when performing upgrades